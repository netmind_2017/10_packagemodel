package com.example.gelso.Bancomat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.gelso.bank.R;

public class Withdraw extends AppCompatActivity {

    Button btn_withdraw,btn_cancel;
    EditText txt_money_withdraw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        layout_setting();
    }

    public void layout_setting() {
        //BTN DEPOSIT
        btn_withdraw = (Button) findViewById(R.id.btn_OKwithdraw);
        btn_cancel = (Button) findViewById(R.id.btn_CANCELwithdraw);
        txt_money_withdraw = (EditText) findViewById(R.id.txt_money_withdraw);

        btn_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = getIntent();
                i.putExtra("money", txt_money_withdraw.getText().toString());
                setResult(RESULT_OK,i);
                finish();

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
}
