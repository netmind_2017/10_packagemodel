package com.example.gelso.Bancomat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.gelso.Bancomat.model.Bancomat;
import com.example.gelso.bank.R;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    //PUBLIC OBJECTS
    Button btn_deposit,btn_withdraw;
    TextView lbl_balance;
    Button btn_logout;
    Bancomat x;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //LAYOUT & FindViewByIds
        layout_settings();

        //DEFINE SHAREPREFERENCE OBJECT
        sharedPref = getPreferences(Context.MODE_PRIVATE);

        //DEFINE MODEL
        x = new Bancomat(sharedPref.getInt("pref_balance",0));

        //UPDATE BALANCE
        updateBalance(x.GetBalance());

       }

    //MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //ACTIVITY RESULT
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to

        if (requestCode == 1) {  //DEPOSIT
            if (resultCode == RESULT_OK) {
                String money = data.getExtras().getString("money");
                x.PutMoney(Integer.parseInt(money));
                updateBalance(Integer.parseInt(money));
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(getApplicationContext(), "Alert: Deposit operation has not been completed. ", Toast.LENGTH_SHORT).show();
            }
        }


        if (requestCode == 2) {  //WITHDRAW
            if (resultCode == RESULT_OK) {
                String money = data.getExtras().getString("money");
                x.Withdraw(Integer.parseInt(money));
                updateBalance(Integer.parseInt(money));
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(getApplicationContext(), "Alert: Withdraw operation has not been completed. ", Toast.LENGTH_SHORT).show();
            }
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_credits) {
            Toast.makeText(getApplicationContext(), "Credits has been clicked ", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public  void layout_settings () {

        //findViewByIds
        lbl_balance = (TextView) findViewById(R.id.lbl_balance);
        btn_deposit = (Button) findViewById(R.id.btn_deposit);
        btn_withdraw = (Button) findViewById(R.id.btn_withdraw);
        btn_logout = (Button) findViewById(R.id.btn_logout);

        //CHANGE TITLE ACTIVITY
        getSupportActionBar().setTitle("Cash machine");

        //CHANGE ICON ACTIVITY
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.money);

        //BTN DEPOSIT
        btn_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ACTIVITY RESULT
                Intent i = new Intent(getApplicationContext(), Deposit.class);
                startActivityForResult(i,1);
            }
        });


        //BTN WITHDRAW
        btn_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ACTIVITY RESULT
                Intent i = new Intent(getApplicationContext(), Withdraw.class);
                startActivityForResult(i,2);
            }
        });


        //BTN LOGOUT
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TOAST
                Toast.makeText(getApplicationContext(), "Bye Bye", Toast.LENGTH_LONG).show();
                //QUIT
                finish();
            }
        });

    }

    public  void updateBalance(int balance) {

        //COMMIT PREFERENCE
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("pref_balance", balance);
        editor.commit();

        //PRINT PREFERENCE & FORMAT NUMBER
        DecimalFormat formatter = new DecimalFormat("#,###");
        lbl_balance.setText("Balance: " + formatter.format(x.GetBalance()) + " Euro");
    }

}
